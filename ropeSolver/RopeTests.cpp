//
//  RopeTests.cpp
//  ropeSolver
//
//  Created by Roberto Sartori on 11/04/16.
//  Copyright © 2016 Roberto Sartori. All rights reserved.
//

#include "RopeTests.hpp"
#include "RopeSolver.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>


#define TEST_PATH "/Users/robertosartori/Development/ropeSolver/test/"

void RopeTests::run() {
    
    std::ifstream ifs;
    
    int iProblem = 0;
    while (true) {
        
        // solver
        RopeSolver solver;
        
        // read rope from file
        std::stringstream fname("");
        fname << TEST_PATH << "rope_" << iProblem++ << ".txt";
        ifs.open(fname.str());
        
        if (ifs.good()) {
            
            int iNode = 0;
            while (!ifs.eof()) {
                int topNodes[2];
                int bottomNodes[2];
                
                ifs >> topNodes[0] >> topNodes[1] >> bottomNodes[0] >> bottomNodes[1];
                
                Node n(iNode++, topNodes, bottomNodes);
                solver.addNode(n);
            }
            
            ifs.close();
            
            // run solution
            solver.solveProblem();
            
            if (solver.ropeState == RopeSolver::ROPE_STATE_OPEN) {
                std::cout << "Case " << iProblem << ": straightened" << std::endl << std::endl;
            } else {
                std::cout << "Case " << iProblem << ": knotted" << std::endl << std::endl;
            }
        } else {
            // no more tests available
            return;
        }
    }
    
}