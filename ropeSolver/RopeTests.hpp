//
//  RopeTests.hpp
//  ropeSolver
//
//  Created by Roberto Sartori on 11/04/16.
//  Copyright © 2016 Roberto Sartori. All rights reserved.
//

#ifndef RopeTests_hpp
#define RopeTests_hpp

#include <stdio.h>

class RopeTests {
public:
    void run();
};

#endif /* RopeTests_hpp */
