//
//  Node.hpp
//  nodeSolver
//
//  Created by Roberto Sartori on 05/04/16.
//  Copyright © 2016 Roberto Sartori. All rights reserved.
//

#ifndef Node_hpp
#define Node_hpp

#include <stdio.h>

class Node {
public:
    int id;
    int topLinks[2];
    int bottomLinks[2];
    
    int lowestIndex();
    int highestIndex();
    
    Node();
    Node(const Node &node);
    Node(int id, int *topLinks, int *bottomLinks);
    
    void operator=(const Node& node);
};

#endif /* Node_hpp */