//
//  RopeSolver.hpp
//  ropeSolver
//
//  Created by Roberto Sartori on 10/04/16.
//  Copyright © 2016 Roberto Sartori. All rights reserved.
//

#ifndef RopeSolver_h
#define RopeSolver_h

#include "RopeReducer.hpp"
#include <vector>
#include <map>
#include <set>


class RopeSolver {
    
private:
    int searchLevel;
    void printIndentation();
    
public:

    typedef enum {
        ROPE_STATE_OPEN,
        ROPE_STATE_CLOSED,
        ROPE_STATE_UNKNOWN
    } ROPE_STATE;
    
    typedef struct {
        int link;
        int pivotId;
        
        int aId;
        int bId;
    } slideMoveType;
    
    
    // starting and ending link indexes
    int startingLink;
    int endingLink;
    std::map<int, Node *> _ropeNodes;
    ROPE_STATE ropeState;
    //std::map<int, Node *>tempMap;
    
    // reduce operations
    RopeReducer *_reducer;
    
    // rope update
    void updateNodeIndexes(std::map<int, Node *> &nodes, int fromIndex, int toIndex);
    
    // Problem setting
    void clearProblem();         // clear the rope problem
    void addNode(Node &node);    // add a new node
    
    // solve operations
    bool reduceSimpleCross(std::map<int, Node *> &nodes);
    bool reduceDoubleCross(std::map<int, Node *> &nodes);
    
    void findSlideMovesWithBaseNodes(std::map<int, Node *> &nodes, std::set<int> &searchNodeIndexes, Node &nodeA, Node &nodeB, std::vector<slideMoveType> &slideMoves);
    void findSlideMoves(std::map<int, Node *> &nodes, std::vector<slideMoveType> &slideMoves);
    bool reduceRope(std::map<int, Node *> &nodes);
    void makeStep(std::map<int, Node *> &nodes, std::vector<slideMoveType> &prevMoves);
    void makeStep(std::map<int, Node *> &nodes);
    void solveProblem();
    
    RopeSolver();
    ~RopeSolver();
    
    
private:
    
    void duplicateNodes(std::map<int, Node *> &nodes, std::map<int, Node *> &newNodeList);
    void releaseNodes(std::map<int, Node *> &nodes);

    
};
#endif /* RopeSolver_h */
