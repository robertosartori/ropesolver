//
//  RopeSolver.cpp
//  nodeSolver
//
//  Created by Roberto Sartori on 05/04/16.
//  Copyright © 2016 Roberto Sartori. All rights reserved.
//

#include "RopeSolver.hpp"
#include <algorithm>
#include <iostream>

RopeSolver::RopeSolver() {
    _reducer = new RopeReducer();
    clearProblem();
}

RopeSolver::~RopeSolver() {
    clearProblem();
}


// PRIVATE

void RopeSolver::printIndentation() {
    for (int i=0; i < searchLevel; i++) {
        std::cout << "    ";
    }
}

// PUBLIC

void RopeSolver::updateNodeIndexes(std::map<int, Node *> &nodes, int fromIndex, int toIndex) {
    
    // always go from the lower to the higher
    if (fromIndex > toIndex) {
        std::swap(fromIndex, toIndex);
    }
    
    // if from is the starting node than exchange
    if (fromIndex == startingLink) {
        fromIndex = toIndex;
        toIndex = startingLink;
    }
    
    for (std::map<int, Node *>::iterator it = nodes.begin(); it != nodes.end(); it++) {
        Node *node = it->second;
        
        if (node->topLinks[0] == fromIndex) {
            node->topLinks[0] = toIndex;
        }
        if (node->topLinks[1] == fromIndex) {
            node->topLinks[1] = toIndex;
        }
        
        if (node->bottomLinks[0] == fromIndex) {
            node->bottomLinks[0] = toIndex;
        }
        
        if (node->bottomLinks[1] == fromIndex) {
            node->bottomLinks[1] = toIndex;
        }
    }
}

void RopeSolver::clearProblem() {
    releaseNodes(_ropeNodes);
    _ropeNodes.clear();
    
    startingLink = INT_MAX;
    endingLink = INT_MIN;
    ropeState = ROPE_STATE_UNKNOWN;
}

void RopeSolver::addNode(Node &node) {
    _ropeNodes[node.id] = new Node(node);
    startingLink = std::min(startingLink, node.lowestIndex());
    endingLink = std::max(endingLink, node.highestIndex());
}


bool RopeSolver::reduceSimpleCross(std::map<int, Node *> &nodes) {
    // reduce all nodes
    for (std::map<int, Node *>::iterator it = nodes.begin(); it != nodes.end(); it++) {
        
        Node *node = it->second;
        
        if (_reducer->isSimpleCross(*node)) {
            
            int equivalentLinks[2];
            _reducer->reduceSimpleCross(*node, equivalentLinks);
            
#ifdef DEBUG
            printIndentation();
            std::cout << "node <" << node->id << "> de-twisted" << std::endl;
#endif
            // mark the end if starting and ending links are discovered to be equivalend
            if (equivalentLinks[0] == startingLink && equivalentLinks[1] == endingLink) {
                ropeState = ROPE_STATE_OPEN;
            }
            
            // node is twisted, reduce it
            updateNodeIndexes(nodes, equivalentLinks[0], equivalentLinks[1]);
            
            // remove the twisted node, if no more nodes with this root exist then remove the array node from the map
            delete nodes.at(node->id);
            nodes.erase(node->id);
            
            return true;
        }
    }
    
    return false;
}

bool RopeSolver::reduceDoubleCross(std::map<int, Node *> &nodes) {
    // reduce all nodes
    for (std::map<int, Node *>::iterator i = nodes.begin(); i != nodes.end(); i++) {
        std::map<int, Node *>::iterator nextI = i;
        nextI++;
        for (std::map<int, Node *>::iterator j = nextI;  j != nodes.end(); j++) {
            Node *nodeA = i->second;
            Node *nodeB = j->second;
            
            if (_reducer->isDoubleCross(*nodeA, *nodeB)) {
                
                // reduce doble cross
                int equivalentLinks[4];
                _reducer->reduceDoubleCross(*nodeA, *nodeB, equivalentLinks);
                
#ifdef DEBUG
                printIndentation();
                std::cout << "nodes <" << nodeA->id << ", " << nodeB->id << ">  de-overlap" << std::endl;
#endif
                // mark the end if starting and ending links are discovered to be equivalend
                if ((equivalentLinks[0] == startingLink && equivalentLinks[1] == endingLink) ||
                    (equivalentLinks[2] == startingLink && equivalentLinks[3] == endingLink)) {
                    ropeState = ROPE_STATE_OPEN;
                    return true;
                }
                
                updateNodeIndexes(nodes, equivalentLinks[0], equivalentLinks[1]);
                updateNodeIndexes(nodes, equivalentLinks[2], equivalentLinks[3]);
                
                // remove equivalent nodes
                delete nodes.at(nodeA->id);
                delete nodes.at(nodeB->id);
                nodes.erase(j);
                nodes.erase(i);
                
                // update only 1 equivalence
                return true;
            }
        }
    }
    
    return false;
}

void RopeSolver::findSlideMoves(std::map<int, Node *> &nodes, std::vector<slideMoveType> &slideMoves) {
    
    // create a support structure containing some maps between links and nodes
    
    std::map<int, std::vector<int>> topNodes;       // maps the list of top nodes for a gived link index
    std::map<int, std::vector<int>> bottomNodes;    // maps the list of bottom nodes for a gived link index
    std::set<int> availableLinks;
    
    
    for (std::map<int, Node *>::iterator iNode=nodes.begin(); iNode != nodes.end(); iNode++) {
        Node *n = iNode->second;
        
        topNodes[n->topLinks[0]].push_back(n->id);
        topNodes[n->topLinks[1]].push_back(n->id);
        
        bottomNodes[n->bottomLinks[0]].push_back(n->id);
        bottomNodes[n->bottomLinks[1]].push_back(n->id);
        
        availableLinks.insert(n->topLinks[0]);
        availableLinks.insert(n->topLinks[1]);
        availableLinks.insert(n->bottomLinks[0]);
        availableLinks.insert(n->bottomLinks[1]);
    }

    for (std::set<int>::iterator it = availableLinks.begin(); it != availableLinks.end(); it++) {
        int iLink = *it;
        if (bottomNodes[iLink].size() == 2) {
            
            Node *nodeA = nodes[bottomNodes[iLink][0]];
            Node *nodeB = nodes[bottomNodes[iLink][1]];
            
            // link is under two other links, check if there is another node that closess the slide triangle
            // create the list of nodes connected to the current link's extreme nodes
            std::set<int> searchIndexes;
            std::vector<int> topNodeA1 = topNodes[nodeA->topLinks[0]];
            std::vector<int> topNodeA2 = topNodes[nodeA->topLinks[1]];
            std::vector<int> topNodeB1 = topNodes[nodeB->topLinks[0]];
            std::vector<int> topNodeB2 = topNodes[nodeB->topLinks[1]];
            
            searchIndexes.insert(&topNodeA1[0], &topNodeA1[0] + topNodeA1.size());
            searchIndexes.insert(&topNodeA2[0], &topNodeA2[0] + topNodeA2.size());
            searchIndexes.insert(&topNodeB1[0], &topNodeB1[0] + topNodeB1.size());
            searchIndexes.insert(&topNodeB2[0], &topNodeB2[0] + topNodeB2.size());
            
            // remove from the search the current link's nodes
            searchIndexes.erase(bottomNodes[iLink][0]);
            searchIndexes.erase(bottomNodes[iLink][1]);

            // search for the possible links
            for (std::set<int>::iterator it = searchIndexes.begin(); it != searchIndexes.end(); it++) {
                
                Node *checkNode = nodes.at(*it);
                Node *a, *b, *c;
                if (_reducer->isSlidePossible(*nodeA, *nodeB, *checkNode, a, b, c)) {
                    
                    slideMoveType slideMove;
                    slideMove.link = iLink;
                    slideMove.pivotId = c->id;
                    
                    if (a->id == nodeA->id) {
                        slideMove.aId = a->id;
                        slideMove.bId = b->id;
                    } else {
                        slideMove.aId = b->id;
                        slideMove.bId = a->id;
                    }
                    
                    slideMoves.push_back(slideMove);
                }
            }
        }
    }
}

bool RopeSolver::reduceRope(std::map<int, Node *> &nodes) {
    // perform a reduction while is't possible
    bool atLeastOneReductionHasDone = false;
    bool reductionDone = true;
    while(reductionDone) {
        reductionDone = false;
        
        reductionDone |= reduceSimpleCross(nodes);
        if (ropeState == ROPE_STATE_OPEN) {
            return true;
        }
        if (!reductionDone) {
            reductionDone |= reduceDoubleCross(nodes);
        }
        
        atLeastOneReductionHasDone |= reductionDone;
        if (ropeState == ROPE_STATE_OPEN) {
            return true;
        }
    }
    
    // check if at least one reduction has been done
    return atLeastOneReductionHasDone;
    
}

void RopeSolver::makeStep(std::map<int, Node *> &nodes, std::vector<slideMoveType> &prevMoves) {
    
    // increase search level
    searchLevel++;
    
    // reduce the rope
    reduceRope(nodes);
    
    if (ropeState == ROPE_STATE_OPEN) {
        searchLevel--;
        return;
    } else {
        
        // rope problem is not yet solved, get the list of links that can be slided
        std::vector<slideMoveType> slideMoves;          // maps the list of possible slide moves
        std::vector<slideMoveType> residualSlideMoves;  // maps the list of
        
        // find the list of possible slide moves
        findSlideMoves(nodes, slideMoves);
        
        // recursively go through the one after which a reduction is possible
        for (int iSlideMove=0; iSlideMove < slideMoves.size(); iSlideMove++) {
            std::map<int, Node *> newNodes;
            duplicateNodes(nodes, newNodes);
            
            // make a slide move
            slideMoveType *slideMove = &(slideMoves.at(iSlideMove));
            Node *nodeA = newNodes[slideMove->aId];
            Node *nodeB = newNodes[slideMove->bId];
            Node *nodeC = newNodes[slideMove->pivotId];
            _reducer->makeSlide(*nodeA, *nodeB, *nodeC);
            
#ifdef DEBUG
            printIndentation();
            std::cout << "slide link " << slideMove->link << " over node <" << slideMove->pivotId << ">" << std::endl;
#endif
            
            // try s reduction steo
            searchLevel++;
            if (reduceRope(newNodes)) {
                // a reduction has been possible!
                
                // check if problem has been solved
                if (ropeState == ROPE_STATE_OPEN) {
                    searchLevel--;
                    return;
                }
                
                // go through recursively resetting the past slide moves
                std::vector<slideMoveType> newMoves;
                makeStep(newNodes, newMoves);
                
                // check if problem has been solved
                if (ropeState == ROPE_STATE_OPEN) {
                    searchLevel--;
                    return;
                }
            } else {
                // reduction has not been possible, queue the slide move
                residualSlideMoves.push_back(*slideMove);
                
#ifdef DEBUG
                printIndentation();
                std::cout << "this slide did not reduced the rope, go deep on this slide later." << std::endl;
#endif
            }
            searchLevel--;
        }
        
        // if step is here then no solution has been yet found, proceed with the residual slide moves avoiding to repeat past moves
        
        for (int iSlideMove = 0; iSlideMove < residualSlideMoves.size(); iSlideMove++) {
        
            slideMoveType slideMove = residualSlideMoves.at(iSlideMove);
            
            // discard already done moves
            bool alreadyDone = false;
            for (int iPrevSlideMove = 0; iPrevSlideMove < prevMoves.size(); iPrevSlideMove++) {
                slideMoveType prevSlideMove = prevMoves.at(iPrevSlideMove);
                
                if (slideMove.link == prevSlideMove.link && slideMove.pivotId == prevSlideMove.pivotId) {
                    alreadyDone = true;
                    break;
                }
            }
            if (alreadyDone)
                continue;
            
#ifdef DEBUG
            printIndentation();
            std::cout << "residual slide link " << slideMove.link << " over node <" << slideMove.pivotId << ">" << std::endl;
#endif
            
            // do the slide move and go through recursively
            std::map<int, Node *> newNodes;
            duplicateNodes(nodes, newNodes);
            
            // make a slide move
            Node *nodeA = newNodes[slideMove.aId];
            Node *nodeB = newNodes[slideMove.bId];
            Node *nodeC = newNodes[slideMove.pivotId];
            _reducer->makeSlide(*nodeA, *nodeB, *nodeC);
            
            prevMoves.push_back(slideMove);
            makeStep(newNodes, prevMoves);
            prevMoves.pop_back();
        }
    }
    
    // if step is here then no solutions have been done for such rope configuartion
    searchLevel--;
}

void RopeSolver::solveProblem() {
    
    std::vector<slideMoveType> slideMoves;
    makeStep(_ropeNodes, slideMoves);
    
    if (ropeState == ROPE_STATE_UNKNOWN) {
        ropeState = ROPE_STATE_CLOSED;
    }
}

// PRIVATE

void RopeSolver::duplicateNodes(std::map<int, Node *> &nodes, std::map<int, Node *> &newNodeList) {
    // duplicate all nodes
    for (std::map<int, Node *>::iterator it = nodes.begin(); it != nodes.end(); it++) {
        Node *node = it->second;
        newNodeList[node->id] = new Node(*node);
    }
}

void RopeSolver::releaseNodes(std::map<int, Node *> &nodes) {
    for (std::map<int, Node *>::iterator it = nodes.begin(); it != nodes.end(); it++) {
        delete it->second;
    }
    
}

