//
//  main.cpp
//  ropeSolver
//
//  Created by Roberto Sartori on 09/04/16.
//  Copyright © 2016 Roberto Sartori. All rights reserved.
//

#include <iostream>
#include "RopeTests.hpp"

int main(int argc, const char * argv[]) {
    // insert code here...
    RopeTests T;
    
    T.run();
    
    return 0;
}
