//
//  RopeParser.hpp
//  ropeSolver
//
//  Created by Roberto Sartori on 11/04/16.
//  Copyright © 2016 Roberto Sartori. All rights reserved.
//

#ifndef RopeParser_hpp
#define RopeParser_hpp

#include <stdio.h>
#include <vector>
#include "RopeSolver.cpp"


class RopeParser {
private:
    
    RopeSolver _solver;
    std::vector<std::vector<char>> _ropeSchema;
    int startingRow, startingCol;
    void parseSchema();
    
public:
    void runParser();
};

#endif /* RopeParser_hpp */
