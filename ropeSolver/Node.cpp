//
//  Node.cpp
//  nodeSolver
//
//  Created by Roberto Sartori on 05/04/16.
//  Copyright © 2016 Roberto Sartori. All rights reserved.
//

#include "Node.hpp"
#include <algorithm>

Node::Node() {
    id = 0;
    topLinks[0] = 0;
    topLinks[1] = 0;
    bottomLinks[0] = 0;
    bottomLinks[1] = 0;
}

Node::Node(const Node &node) {
    this->id = node.id;
    
    this->topLinks[0] = node.topLinks[0];
    this->topLinks[1] = node.topLinks[1];
    
    this->bottomLinks[0] = node.bottomLinks[0];
    this->bottomLinks[1] = node.bottomLinks[1];
}

Node::Node(int id, int *topLinks, int *bottomLinks) {
    this->id = id;
    
    this->topLinks[0] = topLinks[0];
    this->topLinks[1] = topLinks[1];
    
    this->bottomLinks[0] = bottomLinks[0];
    this->bottomLinks[1] = bottomLinks[1];
}

int Node::lowestIndex() {
    return std::min(std::min(topLinks[0], topLinks[1]),
                    std::min(bottomLinks[0], bottomLinks[1]));
}

int Node::highestIndex() {
    return std::max(std::max(topLinks[0], topLinks[1]),
                    std::max(bottomLinks[0], bottomLinks[1]));
}

void Node::operator=(const Node& node) {
    id = node.id;
    
    topLinks[0] = node.topLinks[0];
    topLinks[1] = node.topLinks[1];
    
    bottomLinks[0] = node.bottomLinks[0];
    bottomLinks[1] = node.bottomLinks[1];
}
