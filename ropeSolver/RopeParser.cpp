//
//  RopeParser.cpp
//  ropeSolver
//
//  Created by Roberto Sartori on 11/04/16.
//  Copyright © 2016 Roberto Sartori. All rights reserved.
//

#include "RopeParser.hpp"
#include <iostream>

// some locals
typedef enum {
    ROPE_DIRECTION_TOP,
    ROPE_DIRECTION_BOTTOM,
    ROPE_DIRECTION_LEFT,
    ROPE_DIRECTION_RIGHT
    
} ROPE_DIRECTION;


void RopeParser::parseSchema() {
    // find the first border node
    // 1. search on the first row
}

void RopeParser::runParser() {
    // parse problem statements from std::cin
    
    int iSchema=0;
    while(iSchema < 80) {
        
        // reset solver
        _solver.clearProblem();
        _ropeSchema.clear();
        
        // parse the schema
        int r,c;
        std::vector<std::vector<char>> _ropeSchema;
        
        std::cin >> r >> c;
        
        for (int iRow =0; iRow < r; iRow++) {
            std::string line;
            
            std::cin >> line;
            
            char *lineChars = (char *)line.c_str();
            
            for (int iCol = 0; iCol < c; iCol++) {
                _ropeSchema[iRow].push_back(lineChars[iCol]);
            }
        }
        
        // schema loaded, parse it
        parseSchema();
        
        _solver.solveProblem();
        
        if (_solver.ropeState == RopeSolver::ROPE_STATE_OPEN) {
            std::cout << "Case " << iSchema << ": straightened" << std::endl;
        } else {
            std::cout << "Case " << iSchema << ": knotted" << std::endl;
        }
        
        // count next schema
        iSchema++;
    }
}