//
//  RopeReducer.cpp
//  ropeSolver
//
//  Created by Roberto Sartori on 05/04/16.
//  Copyright © 2016 Roberto Sartori. All rights reserved.
//

#include "RopeReducer.hpp"
#include <algorithm>

// graph topology analysis
bool RopeReducer::isSimpleCross(Node &node) {
    // smple cross condition: the same link appears on both top and bottom in the same node
    return nodeContainsLinkOnBottom(node, node.topLinks[0]) || nodeContainsLinkOnBottom(node, node.topLinks[1]);
}

bool RopeReducer::isDoubleCross(Node &a, Node &b) {
    // double cross condition: two nodes shares both a top link and a bottom link
    bool topLinkIsShared, bottomLinkIsShared;
    
    topLinkIsShared = nodeContainsLinkOnTop(b, a.topLinks[0]) || nodeContainsLinkOnTop(b, a.topLinks[1]);
    bottomLinkIsShared = nodeContainsLinkOnBottom(b, a.bottomLinks[0]) || nodeContainsLinkOnBottom(b, a.bottomLinks[1]);
    
    return topLinkIsShared && bottomLinkIsShared;
}

bool RopeReducer::isSlidePossible(Node &n1, Node &n2, Node &n3, Node* &a, Node* &b, Node* &c) {
    // slide condition is that two nodes should share the same top, two should share the same bottom and two should share a mixed link
    // convention:
    // A shares a bottom link with B
    // B shares a top link with C
    // A shares a mixed link with C
    if (nodesShareABottomLink(n1, n2)) {
        a = &n1;
        b = &n2;
        c = &n3;
    } else if (nodesShareABottomLink(n1, n3)) {
        a = &n1;
        b = &n3;
        c = &n2;
    } else if (nodesShareABottomLink(n2, n3)) {
        a = &n2;
        b = &n3;
        c = &n1;
    } else {
        // no top link shared, slide is not possible
        return false;
    }
    
    // look for the node that shares the top link
    if (nodesShareATopLink(*a, *c)) {
        // it's b that shares the top link
        std::swap(a,b);
    }
    
    // a must share a bottom lilnk with C
    if (!nodesShareAMixedLink(*a, *c))
        return false;
    
    return true;
}

// graph topology reductions
void RopeReducer::reduceSimpleCross(Node &node, int *equivalentLinks) {
    int equivalentTopNodeIndex;
    int equivalentBottomNodeIndex;
    
    // nothing to be reduced indeed, returns the equivlent nodes
    if (nodeContainsLinkOnBottom(node, node.topLinks[0])) {
        equivalentTopNodeIndex = 1;
    } else {
        equivalentTopNodeIndex = 0;
    }
    
    if (nodeContainsLinkOnTop(node, node.bottomLinks[0])) {
        equivalentBottomNodeIndex = 1;
    } else {
        equivalentBottomNodeIndex = 0;
        
    }

    // save equivalent link indexes
    equivalentLinks[0] = node.topLinks[equivalentTopNodeIndex];
    equivalentLinks[1] = node.bottomLinks[equivalentBottomNodeIndex];
    
    if (equivalentLinks[0] > equivalentLinks[1]) {
        std::swap(equivalentLinks[0], equivalentLinks[1]);
    }
}

void RopeReducer::reduceDoubleCross(Node &a, Node &b, int *equivalentLinks) {
    int equivalentTopNodeIndexA;
    int equivalentTopNodeIndexB;
    int equivalentBottomNodeIndexA;
    int equivalentBottomNodeIndexB;

    if (nodeContainsLinkOnTop(a, b.topLinks[0])) {
        equivalentTopNodeIndexB = 1;
    } else {
        equivalentTopNodeIndexB = 0;
    }
    if (nodeContainsLinkOnTop(b, a.topLinks[0])) {
        equivalentTopNodeIndexA = 1;
    } else {
        equivalentTopNodeIndexA = 0;
    }
    
    if (nodeContainsLinkOnBottom(a, b.bottomLinks[0])) {
        equivalentBottomNodeIndexB = 1;
    } else {
        equivalentBottomNodeIndexB = 0;
    }
    if (nodeContainsLinkOnBottom(b, a.bottomLinks[0])) {
        equivalentBottomNodeIndexA = 1;
    } else {
        equivalentBottomNodeIndexA = 0;
    }
    
    // save equivalent link indexes, mantain sorted
    equivalentLinks[0] = a.topLinks[equivalentTopNodeIndexA];
    equivalentLinks[1] = b.topLinks[equivalentTopNodeIndexB];
    equivalentLinks[2] = a.bottomLinks[equivalentBottomNodeIndexA];
    equivalentLinks[3] = b.bottomLinks[equivalentBottomNodeIndexB];
    
    if (equivalentLinks[0] > equivalentLinks[1]) {
        std::swap(equivalentLinks[0], equivalentLinks[1]);
    }
    if (equivalentLinks[2] > equivalentLinks[3]) {
        std::swap(equivalentLinks[2], equivalentLinks[3]);
    }
}

void RopeReducer::makeSlide(Node &a, Node &b, Node &c) {
    // perform index exchanges
    int tmp[2];
    tmp[0] = c.topLinks[0];
    tmp[1] = c.topLinks[1];
    
    c.topLinks[0] = b.topLinks[0];
    c.topLinks[1] = b.topLinks[1];
    
    b.topLinks[0] = c.bottomLinks[0];
    b.topLinks[1] = c.bottomLinks[1];
    
    c.bottomLinks[0] = a.topLinks[0];
    c.bottomLinks[1] = a.topLinks[1];
    
    a.topLinks[0] = tmp[0];
    a.topLinks[1] = tmp[1];
}

// aux members
bool RopeReducer::nodeContainsLinkOnTop(Node &node, int linkIndex) {
    return node.topLinks[0] == linkIndex || node.topLinks[1] == linkIndex;
}

bool RopeReducer::nodeContainsLinkOnBottom(Node &node, int linkIndex) {
    return node.bottomLinks[0] == linkIndex || node.bottomLinks[1] == linkIndex;
}

bool RopeReducer::nodesShareATopLink(Node &nodeA, Node &nodeB) {
    // one top link should be on both top links nodes
    return nodeContainsLinkOnTop(nodeA, nodeB.topLinks[0]) || nodeContainsLinkOnTop(nodeA, nodeB.topLinks[1]);
}

bool RopeReducer::nodesShareABottomLink(Node &nodeA, Node &nodeB) {
    // one bottom link should be on both bottom links nodes
    return nodeContainsLinkOnBottom(nodeA, nodeB.bottomLinks[0]) || nodeContainsLinkOnBottom(nodeA, nodeB.bottomLinks[1]);
}

bool RopeReducer::nodesShareAMixedLink(Node &nodeA, Node &nodeB) {
    // one of the top link should be one of the bottom in the other node
    bool topOnNodeA, topOnNodeB;
    
    topOnNodeA = nodeContainsLinkOnBottom(nodeA, nodeB.topLinks[0]) || nodeContainsLinkOnBottom(nodeA, nodeB.topLinks[1]);
    topOnNodeB = nodeContainsLinkOnBottom(nodeB, nodeA.topLinks[0]) || nodeContainsLinkOnBottom(nodeB, nodeA.topLinks[1]);
    return topOnNodeA || topOnNodeB;
}