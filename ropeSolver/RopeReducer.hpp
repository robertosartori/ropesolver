//
//  RopeReducer.hpp
//  nodeSolver
//
//  Created by Roberto Sartori on 05/04/16.
//  Copyright © 2016 Roberto Sartori. All rights reserved.
//

#ifndef RopeReducer_hpp
#define RopeReducer_hpp

#include <stdio.h>
#include "Node.hpp"

class RopeReducer {
public:
    // graph topology analysis
    bool isSimpleCross(Node &node);
    bool isDoubleCross(Node &a, Node &b);
    bool isSlidePossible(Node &n1, Node &n2, Node &n3, Node* &a, Node* &b, Node* &c);
    
    // graph topology reductions
    void reduceSimpleCross(Node &node, int *equivalentLinks);
    void reduceDoubleCross(Node &a, Node &b, int *equivalentLinks);
    void makeSlide(Node &a, Node &b, Node &c);
    
    // aux members
    bool nodeContainsLinkOnTop(Node &node, int linkIndex);
    bool nodeContainsLinkOnBottom(Node &node, int linkIndex);
    bool nodesShareATopLink(Node &nodeA, Node &nodeB);
    bool nodesShareABottomLink(Node &nodeA, Node &nodeB);
    bool nodesShareAMixedLink(Node &nodeA, Node &nodeB);
};


#endif /* RopeReducer_hpp */